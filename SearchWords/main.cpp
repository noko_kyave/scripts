/* fgets example */
#include <stdio.h>
#include <vector>

int main()
{
   FILE * pFile;
   char mystring[1000];

   pFile = fopen ("test.txt" , "r");
   if (pFile == NULL) perror ("Error opening file");
   else {
     while ( fgets (mystring , 1000 , pFile) != NULL )
       puts (mystring);
     fclose (pFile);
   }
   return 0;
}
