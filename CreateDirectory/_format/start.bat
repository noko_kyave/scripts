@echo off

set /P caseid="input caseId -> "

rem --------- start.bat script ---------

set year=%date:~0,4%
set month=%date:~5,2%
set day=%date:~8,2%
set main_path=%0\..\..
set template_path=%main_path%\_format\Template
set tools_path=%main_path%\_format\Tools
set month_path=%main_path%\%year%_%month%
set case_path=%month_path%\%caseId%

call :confirm_rule
call :create_month_folder
call :create_case_folder
call :copy_tmplate
call :copy_tools
call :rename_files
PAUSE

exit

:confirm_rule
	set /P check="caseID registered? (y/n)->"
	if %check% == y (
		echo "OK"
	) else (
		echo "Please regist caseID in Outlook Rules"
		PAUSE
		exit
	)
exit /b


:create_month_folder
	cd %main_path%
	if exist %year%_%month% (
		rem none
	) else (
		mkdir  %year%_%month%
		mkdir %year%_%month%\_bank
		echo "Create month folder : %year%_%month%"
	)
exit /b

:create_case_folder
	cd %month_path%
	if exist %caseId% (
		echo "Case folder is already created""
		PAUSE
		exit
	) else (
		mkdir  %caseId%
		echo "Create case folder :% caseId%"
	)
exit /b

:copy_tmplate
	cd %case_path%
	xcopy /e %template_path% %case_path%
exit /b

:copy_tools
	cd %case_path%

	xcopy /e %tools_path%\syscheck\syscheck.bat %case_path%\Data\Collect_Logs
	xcopy /e %tools_path%\syscheck\syscheck.exe %case_path%\Data\Collect_Logs
	echo "Copy syscheck (Data\Collect_Logs)"

exit /b

:rename_files
	cd %case_path%

	rename contact.txt contact-%caseId%.txt

	set more_ten_cases=false
	set /A cnt=1
	call :count_caseid
	
	cd %case_path%
	if %more_ten_cases% == true (
		rename answer.txt PP_%year:~2,2%%month%%day%_kk2_%cnt%.txt
	) else (
		rename answer.txt PP_%year:~2,2%%month%%day%_kk2_0%cnt%.txt
	)
	copy PP_*.txt ..\_bank
exit /b


:count_caseid
	cd %month_path%\_bank
	
	for %%i in (*.txt) do (
		call :count_number %%i
	)
	echo end
exit /b

:count_number
	set file_name=%1
	set case_name=%file_name:~-20%
	if %case_name:~0,9% == PP_%year:~2,2%%month%%day% (
		if %cnt% == 9 (
			set more_ten_cases=true
		)
		set /A cnt=%cnt%+1
	)
exit /b